keytool -genkey -v -keystore debug.keystore -alias androiddebugkey -keyalg RSA -keysize 2048 -validity 10000
keytool -exportcert -list -v -alias androiddebugkey -keystore ./debug.keystore